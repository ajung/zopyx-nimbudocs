function initCustomActions(editor){
    var newDocumentOverride = editor.getAction("new-document");
    if (newDocumentOverride !== null) {
        newDocumentOverride.invoke = function(character) {
            editor.loadDocumentFromUrl("source/new.html");
        };
    }
    
    var insertTable = editor.getAction("insert-table");
    if (insertTable !== null) {
        insertTable.invoke = function(character) {
            editor.insertContent("<table width=\"100%\"><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>");
        };
    }
}