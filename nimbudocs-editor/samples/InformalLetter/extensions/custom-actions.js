function initCustomActions(editor){
    var newDocumentOverride = editor.getAction("new-document");
    if (newDocumentOverride !== null) {
        newDocumentOverride.invoke = function(character) {
            editor.loadDocumentFromUrl("source/new.html");
        };
    }
    
    var insertDate = editor.getAction("insert-date");
    if (insertDate !== null) {
        insertDate.invoke = function(character) {
            var monthNames = new Array("January", "February", "March", 
                                       "April", "May", "June", 
                                       "July", "August", "September", 
                                       "October", "November", "December");
            var date = new Date();
            var day = date.getDate();
            var month = date.getMonth();
            var year = date.getFullYear();
            editor.insertContent(monthNames[month] + " " + day + ", " + year);
        };
    }
    
    var insertTable = editor.getAction("insert-table-dialog");
    if (insertTable !== null) {
        insertTable.invoke = function(character) {
            editor.insertContent("<table width=\"100%\"><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr></table>");
        };
    }
}