function initCustomActions(nimbudocsEditor) {
    var insertImage = nimbudocsEditor.getAction("insert-image-dialog");
    if (insertImage) {
        insertImage.invoke = function() {
            nimbudocsEditor.setLocked(true);
            
            var imageData = null;
            var dialogId = "customInsertImageDialog";
            var dialogHtml = "<div id='" + dialogId + "' title='Insert Image...'>";
            dialogHtml += "<table>";
            dialogHtml += "<tr>";
            dialogHtml += "<td>";
            dialogHtml += "<div class='preview' style='width: 80pt; height: 100pt; border: 1pt solid gray;'><p style='padding: 1em; text-align: center;'>Please select an image file.</p></div>";
            dialogHtml += "</td><td>";
            dialogHtml += "<input type='file' class='uploadImage'/>";
            dialogHtml += "</td></tr></table></div>";
            
            var dialog = jQuery(dialogHtml);
            
            jQuery("body").append(dialog);
            
            jQuery("#" + dialogId + " .uploadImage").change(function() {
                var oFReader = new FileReader();
                var Filter = /^(?:image\/bmp|image\/cis\-cod|image\/gif|image\/ief|image\/jpeg|image\/jpeg|image\/jpeg|image\/pipeg|image\/png|image\/svg\+xml|image\/tiff|image\/x\-cmu\-raster|image\/x\-cmx|image\/x\-icon|image\/x\-portable\-anymap|image\/x\-portable\-bitmap|image\/x\-portable\-graymap|image\/x\-portable\-pixmap|image\/x\-rgb|image\/x\-xbitmap|image\/x\-xpixmap|image\/x\-xwindowdump)$/i;
                var oFile = jQuery("#" + dialogId + " .uploadImage")[0].files[0];
                
                if (!Filter.test(oFile.type)) { alert("You must select a valid image file!"); return; }
                
                oFReader.onload = function(oFREvent) {
                    imageData = oFREvent.target.result;
                    
                    jQuery("#" + dialogId + " .preview").html("<img alt='photo' style='width: 100%' src='" + imageData + "'/>");
                };
                
                oFReader.readAsDataURL(oFile);
            });
            
            dialog.dialog({
                appendTo: "#" + nimbudocsEditor.getId(),
                resizable: false,
                width: 400,
                modal: true,
                buttons: {
                    Cancel: function() {
                        jQuery(this).dialog("close");
                    },
                    "Insert": function() {
                        if (imageData) {
                            nimbudocsEditor.insertContent("<img src='" + imageData + "'/>");
                        }
                        jQuery(this).dialog("close");
                    }
                },
                close: function() {
                    jQuery(this).dialog("destroy");
                    dialog.remove();
                    nimbudocsEditor.setLocked(false);
                }
            });
        };
    }
}