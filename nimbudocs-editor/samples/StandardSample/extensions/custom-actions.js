function initCustomActions(editor){
    var newDocumentOverride = editor.getAction("new-document");
    if (newDocumentOverride !== null) {
        newDocumentOverride.invoke = function(character) {
            var response = jQuery.get("source/template.html", function(data){
                editor.loadDocument(data);
            });
        };
    }
    
    var sourceViewDialog = editor.getAction("source-view-dialog");
    if (sourceViewDialog !== null) {
        sourceViewDialog.invoke = function() {
            // Make sure that the user can not type in the editor while the dialog is opened.
            editor.setLocked(true);
            var dialogId = "sourceViewDialog";
            
            // Create the dialog html
            var dialog = jQuery("<div></div>");
            dialog.attr("id", dialogId);
            dialog.attr("title", editor.localize("L_SOURCE_VIEW"));
            
            var textArea = jQuery("<textarea></textarea>");
            textArea.css({
                "resize"        : "none",
                "padding"       : "0",
                "border"        : "none",
                "font-family"   : "Courier, monospace",
                "font-size"     : "9pt",
                "width"         : "100%",
                "height"        : "400px"
            });
            dialog.append(textArea);
            
            // Insert the dialog
            jQuery("body").append(dialog);
            
            // Get the source of the opened document 
            var source = editor.getDocument();
            textArea.val(source);
            
            dialog.dialog({
                appendTo: "#" + editor.getId(),
                resizable: false,
                width: 800,
                modal: true,
                buttons: [
                    {
                        text: editor.localize("L_CANCEL_BUTTON"),
                        click: function() {
                            jQuery(this).dialog("close");
                        }
                    },
                    {
                        text: editor.localize("L_APPLY_BUTTON"),
                        click: function() {
                            // Get the edited source
                            var newSource = textArea.val();
                            
                            if (newSource !== "" && newSource !== undefined) {
                                // Load the edited source as a new document
                                editor.loadDocument(newSource, editor.getDocumentBaseUrl());
                            }
                            jQuery(this).dialog("close");
                        }
                    }
                ],
                close: function() {
                    jQuery(this).dialog("destroy");
                    dialog.remove();
                    editor.setLocked(false);
                }
            });
        };
    }
}