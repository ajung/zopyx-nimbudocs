function showSubnav(name) {
    var subnavs = document.querySelectorAll(".subnav");
    var showSubnavs = document.querySelectorAll(".showSubnav");
    var hideSubnav = document.querySelectorAll(".hideSubnav");
    
    for (var i = 0; i < subnavs.length; i++) {
        subnavs[i].style.display = "none";
    }
    
    for (var i = 0; i < showSubnavs.length; i++) {
        showSubnavs[i].style.display = "inline";
    }
    
    for (var i = 0; i < hideSubnav.length; i++) {
        hideSubnav[i].style.display = "none";
    }
    
    var curShowSubnav = document.getElementById(name + "-showSubnav");
    var curHideSubnav = document.getElementById(name + "-hideSubnav");
    var curSubnav = document.getElementById(name + "-subnav");
    
    if (curShowSubnav) {
        curShowSubnav.style.display = "none";
    }
    
    if (curHideSubnav) {
        curHideSubnav.style.display = "inline";
    }
    
    if (curSubnav) {
        curSubnav.style.display = "block";
    }
}

function hideSubnav(name) {
    var subnavs = document.querySelectorAll(".subnav");
    var showSubnavs = document.querySelectorAll(".showSubnav");
    var hideSubnav = document.querySelectorAll(".hideSubnav");
    
    for (var i = 0; i < subnavs.length; i++) {
        subnavs[i].style.display = "none";
    }
    
    for (var i = 0; i < showSubnavs.length; i++) {
        showSubnavs[i].style.display = "inline";
    }
    
    for (var i = 0; i < hideSubnav.length; i++) {
        hideSubnav[i].style.display = "none";
    }
    
    document.getElementById(name + "-showSubnav").style.display = "inline";
    document.getElementById(name + "-hideSubnav").style.display = "none";
}